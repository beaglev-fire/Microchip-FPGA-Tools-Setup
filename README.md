# Microchip-FPGA-Tools-Setup

This script is intended for use on Ubuntu to setup environment variables allowing command line development for PolarFire SoC.

## Executing the script

This script needs to be sourced. Do not forget the leading dot.

```
. ./setup-microchip-tools.sh
```

## Adjusting the script for your installation
By default the script assumes that the tools are installed in your home directory as follows:

```
  Home
    |
    |-Microchip
        |
        |- Libero_SoC_v2023.2
        |- license
        |- Linux_Licensing_Daemon
        |- SoftConsole-v2022.2-RISC-V-747
```

Edit the top of the script to adjust installation directory names if the tools are installed in other locations on your machine or if you are using different versions of the tools.

## Notes about Libero license file
Do not forget to replace *<put.hostname.here>* with *localhost* on the first line of "license.dat".
